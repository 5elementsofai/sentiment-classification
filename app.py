import datetime
import time
from flask import Flask, jsonify, request, send_file
from flask_cors import CORS, cross_origin
import onnxruntime
from keras.preprocessing import sequence
from keras.preprocessing.sequence import pad_sequences
import numpy
# pickle
import pickle

# SENTIMENT
POSITIVE = "POSITIVE"
NEGATIVE = "NEGATIVE"
NEUTRAL = "NEUTRAL"
SENTIMENT_THRESHOLDS = (0.333, 0.666)
SEQUENCE_LENGTH = 300

# MODEL
TOKENIZER = 'tokenizer.pkl'
MODEL = 'model.onnx'

def get_tokenizer():
    with open(TOKENIZER, 'rb') as handle:
        return pickle.load(handle)

def decode_sentiment(score):
    label = NEUTRAL
    if score <= SENTIMENT_THRESHOLDS[0]:
        label = NEGATIVE
    elif score >= SENTIMENT_THRESHOLDS[1]:
        label = POSITIVE

    return label
        
def predict_sentiment(text):
    start_at = time.time()

    # Tokenize text
    x_test = pad_sequences(tokenizer.texts_to_sequences([text]), maxlen=SEQUENCE_LENGTH)

    input_name = sess.get_inputs()[0].name
    label_name = sess.get_outputs()[0].name

    # Get score
    score = sess.run([label_name], {input_name: x_test.astype(numpy.float32)})[0]

    # Return: score + label + time spent
    return decode_sentiment(score), score, time.time()-start_at

# Get tokenizer and ONNX session
tokenizer = get_tokenizer()
sess = onnxruntime.InferenceSession(MODEL)

app = Flask(__name__)
cors = CORS(app)

API_V1 = '/api/1.0'

@app.route(API_V1 + '/ping', methods=['GET'])
def ping():
    return "pong"

@app.route(API_V1 + '/info', methods=['GET'])
def info():
    return jsonify({
        'version': API_V1,
        'project': '5 elements of AI',
        'service': 'sentiment-classification',
        'language': 'python',
        'type': 'api',
        'date': str(datetime.datetime.now()),
    })


@app.route(API_V1 + '/predict', methods=['POST', 'OPTIONS'])
@cross_origin(origin='localhost')
def predict():
    data = request.json
    
    if 'sentence' in data:

        sentence = data['sentence']

        label, score, elapsed_time = predict_sentiment(sentence)

        return jsonify({ 'label': label, 'score': float(score), 'elapsed_time': elapsed_time })

    return 'no data provided'
    

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True, threaded=True)